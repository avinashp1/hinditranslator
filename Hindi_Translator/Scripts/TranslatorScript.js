﻿var scriptPath = getScriptsPath();

LoadScript(scriptPath + "jQuery-BlockUI.js");
LoadScript(scriptPath + "jquery.translate-1.4.7.min.js");
LoadScript(scriptPath + "jquery.cookie.js");

function LoadScript(url) {
    document.write('<scr' + 'ipt type="text/javascript" src="' + url + '"><\/scr' + 'ipt>');
}

function getScriptsPath() {
    var scripts = document.getElementsByTagName("script");
    var regex = /(.*\/)TranslatorScript/i;
    for (var i = 0; i < scripts.length; i++) {
        var currentScriptSrc = scripts[i].src;
        if (currentScriptSrc.match(regex))
            return currentScriptSrc.match(regex)[1];
    }

    return null;
}

jQuery(function($) {
    debugger;
    $('body').prepend("<div id=\"bottom-toolbar\" style=\"color: #fff; background: url('Images/TranslatorImages/TranslateButton_Background.png') repeat scroll 0 0 transparent;\">  <div id=\"translateSpan\"><img src=\"Images/TranslatorImages/Translate_Website.png\" alt=\"\"/> </div> <div id=\"translate-bar\" style=\" color: #fff; background: url('Images/TranslatorImages/TranslateButton_Background.png') repeat scroll 0 0 transparent; padding: 5px;\"> </div> <div id=\"hide\"> <img src=\"Images/TranslatorImages/TranslateBar_Shown_background.png\" class=\"translateButton\" alt=\"\" /> </div></div><div id=\"show\"> <img src=\"Images/TranslatorImages/TranslateBar_closed_background.png\" class=\"translateButton\" alt=\"\" />  </div>");

    $('head').append("<style type=\"text/css\"> #bottom-toolbar   {  bottom: 0; position: fixed; right: 10px; width: 350px; height: 36px; z-index: 99000; } #translate-bar { bottom: 0; height: 26px; position: fixed; right: 7%; top: 96%; width: auto; z-index: 99000; } #hide { bottom: 0;  position: fixed; right: 0.5%; top: 96%; width: auto; z-index: 99000; } #show { bottom: 0; height: 34px; position: fixed; right: 0.5%; top: 96%; width: auto; z-index: 99000; } #translateSpan { padding-top: 7px; padding-left: 10px; right: 0.7%;  } </style>");

    $("#bottom-toolbar").toggle();
    $(".translateButton").click(function() {
        $("#bottom-toolbar").toggle();
        $("#show").toggle();
    });

    try {
        loadTranslator();
    }
    catch (e) {
        alert('Language Translation Error');
    }
});

function loadTranslator() {

    $.translate(function() {

        try {
            $('#translate-bar').html("");
        }
        catch (e) {
        }

        var selectedLanguage = $.cookie('selectedLanguage'); //get previously translated language

        if (selectedLanguage) {
            if(selectedLanguage != 'en')
                translateTo(selectedLanguage);
        }

        function translateTo(selectedLanguage) {
            $('body').translate('english', selectedLanguage, {
                not: '.jq-translate-ui',
                fromOriginal: true,
                start: function() {
                    $('#jq-translate-ui').val(selectedLanguage);
                    $.blockUI.defaults.applyPlatformOpacityRules = false;
                    $.blockUI(
                            {
                                message: 'Language Translation In Progress, Please Wait...',
                                css:
                                {
                                    border: 'none',
                                    padding: '10px',
                                    backgroundColor: '#000',
                                    '-webkit-border-radius': '9px',
                                    '-moz-border-radius': '9px',
                                    opacity: .9,
                                    color: '#fff'
                                },
                                overlayCSS: { backgroundColor: '#000', opacity: 0.6, '-moz-opacity': '0.6', width: '100%', height: '100%' }
                            });
                },
                complete: function() { $.unblockUI({ css: { cursor: 'default'} }); }
            });
        }

        //        $.translate.ui('ul', 'li', 'span')
        //          .appendTo('body')   
        //          .css({ 'color': 'blue', 'background-color': 'white' })
        //          .find('span')
        //          .css('cursor', 'pointer')
        //          .click(function() {   
        //                  translateTo($(this).text());
        //                  $.cookie('selectedLanguage', $(this).text());
        //                  return false;
        //              });
        //

        $.translate.ui({
            tags: ["select", "option"],
            //a function that filters the languages:
            filter: $.translate.isTranslatable, //this can be an array of languages/language codes too
            //a function that returns the text to display based on the language code:
            label: $.translate.toNativeLanguage ||
                function(langCode, lang) { return $.translate.capitalize(lang); },
            //whether to include the UNKNOWN:"" along with the languages:
            includeUnknown: false//,
            //change: (function(){ translateTo($(this).val()); })
        }).change(function() {
            $.cookie('selectedLanguage', $(this).val(), { path: '/' });
            translateTo($(this).val());
            return true;
        }).appendTo('#translate-bar'); //.prependTo('body');
    }); 
}

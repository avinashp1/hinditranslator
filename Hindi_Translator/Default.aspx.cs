﻿using Google.Cloud.Translation.V2;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Hindi_Translator
{
    public partial class Default : System.Web.UI.Page
    {
    
        protected void Page_Load(object sender, EventArgs e)
        {
          string RESPONSES= Mains();
            //     label1.Text = str;
            LABEL1.Text = RESPONSES;
        }
          public  string Mains()
            {
            SqlConnection sConnn = CreateDBConn();
            SqlCommand cmdd = new SqlCommand("select top (2000) * from BankCorresponds where hindi_BankCorrespondName is  null", sConnn);
            sConnn.Open();
            SqlDataReader res = cmdd.ExecuteReader();
            DataTable dt2 = new DataTable();
            dt2.Load(res);

            sConnn.Close();

            for (int i = 0; i < dt2.Rows.Count; i++)
            {
                string id = dt2.Rows[i][0].ToString();
                string name = dt2.Rows[i][1].ToString();
              
                Console.OutputEncoding = System.Text.Encoding.Unicode;
                TranslationClient client = TranslationClient.Create();
                var response = client.TranslateText(name, "hi");
                var hindi_name= response.TranslatedText;
                SqlConnection sConn = CreateDBConn();
                SqlCommand cmd = new SqlCommand("Update BankCorresponds set hindi_BankCorrespondName=N'" + hindi_name.ToString() + "' where BankCorrespondId='" + id + "'",sConn);
                sConn.Open();
                
                cmd.ExecuteNonQuery();
                sConn.Close();

            }
            return "";
           
                  }
        public static DataTable LoadDataTable(string sqlProc, Hashtable parameters)
        {
            try
            {
                DataTable dtRes = new DataTable();
                string[] paramNames = new string[parameters.Keys.Count];
                parameters.Keys.CopyTo(paramNames, 0);
                object[] paramValues = new object[parameters.Values.Count];
                parameters.Values.CopyTo(paramValues, 0);

                SqlConnection sConn = CreateDBConn();
                SqlCommand sCom = new SqlCommand();
                sCom.Connection = sConn;
                SqlParameter[] spram = CreateParameterList(paramNames, paramValues, paramNames.Count());
                foreach (SqlParameter sp in spram)
                {
                    sCom.Parameters.Add(sp);
                }
                sCom.CommandText = sqlProc;
                sCom.CommandType = CommandType.StoredProcedure;

                sCom.CommandTimeout = 0;

                sConn.Open();
                SqlDataReader res = sCom.ExecuteReader();
                dtRes.Load(res);
                sConn.Close();

                return dtRes;
            }
            catch(Exception ex)
            {
                throw ex;
            }
            }

        private static SqlConnection CreateDBConn()
        {
            SqlConnection sConn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ToString());
            return sConn;
        }
      
        private static SqlParameter[] CreateParameterList(string[] paramNames, object[] paramValues, int arrLength)
        {
            SqlParameter[] parameterList = new SqlParameter[arrLength];
            for (int i = 0; i < paramNames.Length; i++)
            {
                if (paramValues[i] == null)
                {
                    paramValues[i] = DBNull.Value;
                }

                parameterList[i] = new SqlParameter(paramNames[i], paramValues[i]);
            }
            return parameterList;
        }
    }
}